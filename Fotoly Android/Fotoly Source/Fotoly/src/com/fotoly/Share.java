/*
 * 
 */
package com.fotoly;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fotoly.custom.CustomActivity;

/**
 * The Class Share is the activity class that will launch after user Capture a
 * Photo or Video from in-app Camera. The current implementation simply shows a
 * static image. You must write your logic to display actual Image or Video
 * thumb. You also need to write your logic for Share on Facebook, Twitter,
 * Instagram and Mail.
 */
public class Share extends CustomActivity
{
	/* (non-Javadoc)
	 * @see com.newsfeeder.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share);

		initViewComponents();
	}

	/**
	 * This method initialize the view components, assign OnClick, OnTouch and
	 * other required listeners on views. It also apply the selected theme on
	 * Views.
	 */
	private void initViewComponents()
	{
		setTouchNClick(R.id.btnDownload);
		setTouchNClick(R.id.btnShare);
		setTouchNClick(R.id.tabFb);
		setTouchNClick(R.id.tabTw);
		setTouchNClick(R.id.tabMail);
		setTouchNClick(R.id.tabIg);

		applyBgTheme(findViewById(R.id.vTop));
		applyBgTheme(findViewById(R.id.vBottom));
	}

	/* (non-Javadoc)
	 * @see com.fotoly.custom.CustomActivity#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{
		super.onClick(v);

		if (v.getId() == R.id.btnDownload)
		{
			finish();
		}
		else if (v.getId() == R.id.btnShare || v.getId() == R.id.tabFb
				|| v.getId() == R.id.tabTw || v.getId() == R.id.tabIg
				|| v.getId() == R.id.tabMail)
		{
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("image/*");
			startActivity(Intent.createChooser(i, "Share via"));
		}

	}

}
