/*
 * 
 */
package com.fotoly;

import android.os.Bundle;
import android.view.Menu;

import com.fotoly.custom.CustomActivity;

/**
 * The Class Timwline is the activity class that shows the Timeline of a user.
 * Currently this activity is launched when user select a Friend from Friend listing screen.
 */
public class Timeline extends CustomActivity
{
	/* (non-Javadoc)
	 * @see com.newsfeeder.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timeline);

		initViewComponents();
	}

	/**
	 * This method initialize the view components, assign OnClick, OnTouch and
	 * other required listeners on views. It also apply the selected theme on
	 * Views.
	 */
	private void initViewComponents()
	{
		getActionBar().setTitle("Timeline");

		applyBgTheme(setTouchNClick(R.id.vLike));
		applyBgTheme(setTouchNClick(R.id.vComment));
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.timeline, menu);
		return super.onCreateOptionsMenu(menu);
	}

}
