/*
 * 
 */
package com.fotoly;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fotoly.custom.CustomActivity;

/**
 * The Class Theme is the activity class that is launched when user tapes on Themes button in Settings screen.
 * This activity allows the user to select the for app.
 */
public class Themes extends CustomActivity
{
	/* (non-Javadoc)
	 * @see com.newsfeeder.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.themes);

		initViewComponents();
	}

	/**
	 * This method initialize the view components, assign OnClick, OnTouch and
	 * other required listeners on views.
	 */
	private void initViewComponents()
	{
		setTouchNClick(R.id.themeBlue);
		setTouchNClick(R.id.themeGreen);
		setTouchNClick(R.id.themeRed);
		setTouchNClick(R.id.themeYellow);

		getActionBar().setTitle("Themes");
	}

	/* (non-Javadoc)
	 * @see com.fotoly.custom.CustomActivity#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.themeBlue)
			saveAppTheme(THEME_BLUE);
		else if (v.getId() == R.id.themeGreen)
			saveAppTheme(THEME_GREEN);
		else if (v.getId() == R.id.themeRed)
			saveAppTheme(THEME_RED);
		else if (v.getId() == R.id.themeYellow)
			saveAppTheme(THEME_YELLOW);
		startActivity(new Intent(this, MainActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		finish();
	}
}
