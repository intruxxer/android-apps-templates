/*
 * 
 */
package com.fotoly;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.fotoly.custom.CustomActivity;
import com.fotoly.model.Data;

/**
 * The Class Friends is an Activity class that shows the Friend list from
 * Facebook, Twitter and Instagram. The current implementation of this class
 * simply shows some friends in list.
 */
public class Friends extends CustomActivity
{

	/** The friends list. */
	private ArrayList<Data> sList;

	/* (non-Javadoc)
	 * @see com.newsfeeder.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friends);

		initViewComponents();
	}

	/**
	 * This method initialize the view components, assign OnClick, OnTouch and
	 * other required listeners on views.
	 */
	private void initViewComponents()
	{
		getFriendList();
		ListView list = (ListView) findViewById(R.id.list);
		final FriendAdapter adp = new FriendAdapter();
		list.setAdapter(adp);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3)
			{
				startActivity(new Intent(Friends.this, Timeline.class));
			}
		});

		setTouchNClick(R.id.btnFb);
		setTouchNClick(R.id.btnTw);
		setTouchNClick(R.id.btnIg);
		getActionBar().setTitle("Friends");
	}

	/**
	 * This method loads the list of friends. The current implementation of this
	 * method simply creates a dummy list. You must write the code to load the
	 * actual listings.0
	 * 
	 */
	private void getFriendList()
	{
		sList = new ArrayList<Data>();
		sList.add(new Data("Steven", "Australia", R.drawable.friend1));
		sList.add(new Data("Santa", "Japan", R.drawable.friend2));
		sList.add(new Data("Renu", "India", R.drawable.friend3));
		sList.add(new Data("Robin", "Russia", R.drawable.friend4));
		sList.add(new Data("Stalin", "Italy", R.drawable.friend5));
		sList.add(new Data("Steven", "Australia", R.drawable.friend1));
		sList.add(new Data("Santa", "Japan", R.drawable.friend2));
		sList.add(new Data("Renu", "India", R.drawable.friend3));
		sList.add(new Data("Robin", "Russia", R.drawable.friend4));
		sList.add(new Data("Stalin", "Italy", R.drawable.friend5));
	}

	/**
	 * The Class FriendAdapter is the adapter class for ListView of this
	 * activity. You can change or add your own logic to render the list view
	 * items.
	 */
	private class FriendAdapter extends BaseAdapter
	{

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return sList.size();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Data getItem(int arg0)
		{
			return sList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return 0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
				convertView = getLayoutInflater().inflate(R.layout.friend_item,
						null);

			Data d = getItem(position);
			TextView lbl = (TextView) convertView.findViewById(R.id.lbl1);
			lbl.setText(d.getTitle());

			lbl = (TextView) convertView.findViewById(R.id.lbl2);
			lbl.setText(d.getDesc());

			ImageView img = (ImageView) convertView.findViewById(R.id.img);
			img.setImageResource(d.getImage());

			return convertView;
		}

	}
}
