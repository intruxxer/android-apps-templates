/*
 * 
 */
package com.fotoly.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.fotoly.MainActivity;
import com.fotoly.R;

/**
 * The Class FilterFragment is the Fragment class that shows the Filter effects to be applied on Photos captured.
 * The current implementation simply shows the different type of effects, you can add your own effects that you want.
 */
public class FilterFragment extends Fragment
{

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.filters, null);

		GridView list = (GridView) v.findViewById(R.id.grid);
		list.setAdapter(new GridAdapter());
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3)
			{
				((MainActivity) getActivity())
						.goBackFromFragment(MainActivity.DISPLAY_PHOTO);
			}
		});
		list.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				((MainActivity) getActivity()).gesture.onTouchEvent(event);
				return false;
			}
		});
		return v;
	}

	/**
	 * The Class GridAdapter is the adapter class for GridView used in this Fragment.
	 * The current implementation holds 9 Grid items with each item holds an ImageView.
	 * You can change this adapter logic as per your need.
	 */
	private class GridAdapter extends BaseAdapter
	{
		
		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return 9;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Object getItem(int arg0)
		{
			return null;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.filter_item, null);

			ImageView img = (ImageView) v;
			if (pos == 0)
				img.setImageResource(R.drawable.filter1);
			else if (pos == 1)
				img.setImageResource(R.drawable.filter2);
			else if (pos == 2)
				img.setImageResource(R.drawable.filter3);
			else if (pos == 3)
				img.setImageResource(R.drawable.filter4);
			else if (pos == 4)
				img.setImageResource(R.drawable.filter5);
			else if (pos == 5)
				img.setImageResource(R.drawable.filter6);
			else if (pos == 6)
				img.setImageResource(R.drawable.filter7);
			else if (pos == 7)
				img.setImageResource(R.drawable.filter8);
			else if (pos == 8)
				img.setImageResource(R.drawable.filter9);
			return v;
		}

	}
}
