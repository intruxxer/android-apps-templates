package com.socialshare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;

/**
 * The Class Elements is the Fragment class that is launched when the user
 * selects the Elements option in Left navigation drawer. It shows the custom
 * useful layout components like custom SeekBar that you can use in your
 * application layouts.
 */
public class Elements extends CustomFragment
{

	/** Check if the app is running. */
	private boolean isRunning;

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.elements, null);

		setDummyContents(v);
		return v;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroyView()
	 */
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		isRunning = false;
	}

	/**
	 * Sets the dummy contents for views. You can remove this method or can
	 * customize this method as per your needs. The thread in this method simply
	 * used to create a progress bar effect for Progress bar.
	 *
	 * @param v the new dummy contents
	 */
	private void setDummyContents(final View v)
	{
		isRunning = true;
		setTouchNClick(v.findViewById(R.id.btnDemo));

		new Thread(new Runnable() {
			@Override
			public void run()
			{
				final ProgressBar pBar = (ProgressBar) v
						.findViewById(R.id.progressBar1);
				final TextView lbl1 = (TextView) v
						.findViewById(R.id.lblProgress1);
				final TextView lbl2 = (TextView) v
						.findViewById(R.id.lblProgress2);
				while (isRunning)
				{
					try
					{
						Thread.sleep(1000);
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					if (getActivity() == null)
					{
						isRunning = false;
						break;
					}
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run()
						{
							int p = pBar.getProgress() + 5;
							if (p > 100)
								p = 0;
							pBar.setProgress(p);
							lbl1.setText(p + "MB/100MB");
							lbl2.setText(p + "%");
						}
					});
				}
			}
		}).start();
	}
}
