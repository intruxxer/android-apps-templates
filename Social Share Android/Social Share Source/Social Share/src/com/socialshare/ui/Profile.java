package com.socialshare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;

/**
 * The Class Profile is the Fragment class that is launched when the user clicks
 * on Profile option in Left navigation drawer.
 * This screen shows user's profile photo and it also shows user's photo and video feeds. 
 */
public class Profile extends CustomFragment
{

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.profile, null);

		ListView list = (ListView) v.findViewById(R.id.list);
		list.setAdapter(new CustomAdapter());
		return v;
	}

	/**
	 * The Class CustomAdapter is the adapter for displaying User's photo and video feeds.
	 * The current implementation simply display dummy photo and video feed. You need
	 * to change it as per your needs.
	 */
	private class CustomAdapter extends BaseAdapter
	{

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return 10;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Object getItem(int arg0)
		{
			return null;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.profile_item, null);
			if (pos % 2 == 0)
			{
				ImageView img = (ImageView) v.findViewById(R.id.img1);
				img.setImageResource(R.drawable.timeline_frame_pic);

				img = (ImageView) v.findViewById(R.id.img2);
				img.setImageResource(R.drawable.feed_img);

				TextView lbl = (TextView) v.findViewById(R.id.lbl1);
				lbl.setText("Stalking");

				lbl = (TextView) v.findViewById(R.id.lbl2);
				lbl.setText("How is winter season treating you?");
			}
			else
			{
				ImageView img = (ImageView) v.findViewById(R.id.img1);
				img.setImageResource(R.drawable.timeline_frame_vid);

				img = (ImageView) v.findViewById(R.id.img2);
				img.setImageResource(R.drawable.feed_vid);

				TextView lbl = (TextView) v.findViewById(R.id.lbl1);
				lbl.setText("Table for two");

				lbl = (TextView) v.findViewById(R.id.lbl2);
				lbl.setText("Somewhere in Chacago, Illinois, USA");
			}
			return v;
		}

	}
}
